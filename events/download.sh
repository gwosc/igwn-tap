# -- All available event list
set -ex

mkdir -p original_data
wget https://www.gw-openscience.org/eventapi/json/allevents/ --output-document original_data/events.json
