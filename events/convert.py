import json
import re
from collections import Counter
from pathlib import Path

from astropy.table import Table

ORIGINAL_DATA_PATH = Path(__file__).parent / 'original_data' / 'events.json'
CONVERTED_DATA_PATH = Path(__file__).parent / 'converted_data' / 'all_events.csv'
CONVERTED_DATA_PATH.parent.mkdir(exist_ok=True)

data = list(json.loads(ORIGINAL_DATA_PATH.read_text())['events'].values())
table = Table(data)
table.rename_column('catalog.shortName', 'catalog')
table.rename_column('GPS', 'geocent_time_gps')
table.rename_column('commonName', 'name')
table['ifar'] = [(1 / _ if _ != 1e-5 else 1e5) if _ is not None else None for _ in table['far']]  # avoid 99999.99999999999 in csv
print('All catalogs:')
print(sorted(set(table['catalog'])))

CATALOG_SPEC_REGEX = re.compile('-(auxiliary|confident|marginal)')
table['release'] = [CATALOG_SPEC_REGEX.sub('', _) for _ in table['catalog']]
table['catalog'][table['catalog'] == 'O3_IMBH_marginal'] = 'O3-IMBH-marginal'

# make the reference always an URL
table['reference'] = [_.removesuffix('/') for _ in table['reference']]
table['reference'] = [_ if _.startswith('http') else f'https://www.gw-openscience.org{_}' for _ in table['reference']]

assert all(_ is None for _ in table['far_lower'])
del table['far_lower']
assert all(_ is None for _ in table['far_upper'])
del table['far_upper']
assert all(_ is None for _ in table['p_astro_lower'])
del table['p_astro_lower']
assert all(_ is None for _ in table['p_astro_upper'])
del table['p_astro_upper']

# check unit columns
print('\nColumn units:')
columns = list(table.columns)
for column in columns:
    if not column.endswith('_unit'):
        continue
    if column == 'far_unit':
        # value or 1/yr and yr^-1 are present
        continue
    if column in {'chi_eff_unit', 'network_matched_filter_snr_unit', 'p_astro_unit', 'redshift_unit'}:
        # '' or None only
        continue
    value = next(_ for _ in table[column] if _ is not None and _ != '')
    print(column, value)
    assert all(_ is None or _ == '' or _ == value for _ in table[column])
# chirp_mass_source_unit M_sun
# chirp_mass_unit M_sun
# final_mass_source_unit M_sun
# luminosity_distance_unit Mpc
# mass_1_source_unit M_sun
# mass_2_source_unit M_sun
# total_mass_source_unit M_sun

columns = [_ for _ in columns if not _.endswith('_unit') and not _.endswith('_lower') and not _.endswith('_upper')]
new_columns = [
    'name',
    'release',
    'catalog',
    'network_matched_filter_snr',
#    'network_matched_filter_snr_lower',
#    'network_matched_filter_snr_upper',
    'chi_eff',
#    'chi_eff_lower',
#    'chi_eff_upper',
    'far',
    'ifar',
    'geocent_time_gps',
    'p_astro',
    'luminosity_distance',
#    'luminosity_distance_lower',
#    'luminosity_distance_upper',
    'redshift',
#    'redshift_lower',
#    'redshift_upper',
    'mass_1_source',
#    'mass_1_source_lower',
#    'mass_1_source_upper',
    'mass_2_source',
#    'mass_2_source_lower',
#    'mass_2_source_upper',
    'total_mass_source',
#    'total_mass_source_lower',
#    'total_mass_source_upper',
    'final_mass_source',
#    'final_mass_source_lower',
#    'final_mass_source_upper',
    'chirp_mass',
#    'chirp_mass_lower',
#    'chirp_mass_upper',
    'chirp_mass_source',
#    'chirp_mass_source_lower',
#    'chirp_mass_source_upper',
    'jsonurl',
    'reference',
    'version',
]
assert set(columns) == set(new_columns)
table = table[new_columns]

# Pick up only the latest version of the event
counters = Counter(table['name'])
valid_entries = set((name, version) for name, version in counters.items())
mask = [(_['name'], _['version']) in valid_entries for _ in table]
table = table[mask]

# Filter out the Initial_LIGO_Virgo, which only contains the events blind_injection and GRB051103 
table = table[table['catalog'] != 'Initial_LIGO_Virgo']

print('\nKept catalogs:')
print(sorted(set(table['catalog'])))

table.sort('geocent_time_gps')

table.write(CONVERTED_DATA_PATH, format='ascii.csv', overwrite=True)
