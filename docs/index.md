This document describes the content of the [IGWN TAP](http://ccwbvps22.in2p3.fr), a public database managed by the [GWOSC](https://www.gw-openscience.org), and the procedures to operate it.

The database currently contains information about gravitational wave events and CBC pipeline triggers. These triggers come from inside and outside the LVK collaboration. It can be accessed through the [Table Access Protocol](https://www.ivoa.net/documents/TAP), an IVOA-recommended protocol, by using Virtual Observatory tools.

The aim of this database is to facilitate

* the access to the event and pipeline trigger lists
* the cross-matches between various searches or between trigger lists and other astronomical catalogs


## Available resources

<style>
table th:first-of-type {
    width: 10%;
}
table th:nth-of-type(2) {
    width: 20%;
}
table th:nth-of-type(3) {
    width: 70%;
}
</style>

+------------+------------------+----------------------------------------------------------------+
| Schema     | Table            | Description                                                    |
+============+==================+=========================================+======================+
| `events`   | `all_events`     | The gravitational wave event compilation table. It contains    |
|            |                  | events from the _GWTC-1_, _GWTC-2.1_, _GWTC-3_ and             |
|            |                  | the _O3 Intermediate Black Hole Marginal_ data releases.       |
+------------+------------------+----------------------------------------------------------------+
| `triggers` | `all_triggers`   | The gravitational wave pipeline trigger compilation table.     |
|            |                  | It contains the triggers published in the _GWTC-2.1_, _GWTC-3_,|
|            |                  | _4-OGC_ and _O3b-IAS_ data releases.                           |
+------------+------------------+----------------------------------------------------------------+


## Usage

The IGWN TAP server can be accessed using VO tools either interactively, or programmatically (e.g., [astropy/PyVO](https://pyvo.readthedocs.io/en/latest)).

The querying of the TAP server is performed using [ADQL](https://www.ivoa.net/documents/ADQL/20180112/PR-ADQL-2.1-20180112.html), a SQL dialect geared towards astronomical dataset queries. We recommend to beginners the reading of the [ADQL cheatsheet](http://tapvizier.u-strasbg.fr/adql/help.html).

The query examples given in this documentation can be tried out using the [TAP query form](http://ccwbvps22.in2p3.fr).
