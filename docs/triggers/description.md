The Pipeline Trigger Database is accessible in the table `triggers.all_triggers`. It contains 1428573 triggers from 4 releases. Two come from the LVK collaboration: GWTC-2.1 and GWTC-3 (Candidate Data Release) and the other two releases are external to it: 4-OGC (Nitz, Alexander et al. 2021) and IAS-O3a (Olsen, Seth et al. 2022).


+---------+------------------------------------------------------------+------------------------+------------+
| Release | Description                                                | Catalog                | \#triggers |
+=========+============================================================+========================+============+
| GWTC-2.1| Deep Extended Catalog of Compact Binary Coalescences       | GWTC-2.1-GSTLAL_AllSky | 405        |
|         | Observed by LIGO and Virgo During the First Half of        +------------------------+------------+
|         | the Third Observing Run — Candidate Data Release. <br>     | GWTC-2.1-MBTA_AllSky   | 320        |
|         | [catalog paper](https://dcc.ligo.org/LIGO-P2100063/public) +------------------------+------------+
|         |                                                            | GWTC-2.1-PYCBC_AllSky  | 337        |
|         |                                                            +------------------------+------------+
|         |                                                            | GWTC-2.1-PYCBC_HighMass| 329        |
+---------+------------------------------------------------------------+------------------------+------------+
| GWTC-3  | Compact Binary Coalescences                                | GWTC-3-GSTLAL_AllSky   | 309        |
|         | Observed by LIGO and Virgo                                 +------------------------+------------+
|         | during the Second Part of the                              | GWTC-3-MBTA_AllSky     | 264        |
|         | Third Observing Run —                                      +------------------------+------------+
|         | Candidate Data Release. <br>                               | GWTC-3-PYCBC_AllSky    | 284        |
|         | [catalog paper](https://dcc.ligo.org/LIGO-P2000318/public) +------------------------+------------+
|         |                                                            | GWTC-3-PYCBC_HighMass  | 325        |
+---------+------------------------------------------------------------+------------------------+------------+
| 4-OGC   | Fourth Open Gravitational-wave Catalog (4-OGC) of binary   | 4-OGC                  | 1424955    |
|         | neutron star (BNS), binary black hole (BBH) and neutron    |                        |            |
|         | star-black hole (NSBH) mergers. The catalog includes       |                        |            |
|         | observations from 2015-2020 covering the first through     |                        |            |
|         | third observing runs (O1, O2, O3a, O3b) of Advanced LIGO   |                        |            |
|         | And Advanced Virgo.                                        |                        |            |
|         | <br>                                                       |                        |            |
|         | [catalog paper](https://arxiv.org/abs/2112.06878)          |                        |            |
+---------+------------------------------------------------------------+------------------------+------------+
| IAS-O3a | Candidates are identified using an updated version of the  | IAS-O3a                | 1045       |
|         | IAS pipeline (Venumadhav et al.), and events are declared  |                        |            |
|         | according to criteria similar to those in the GWTC-2p1     |                        |            |
|         | catalog (Abbott et al.). The updated search is sensitive to|                        |            |
|         | a larger region of parameter space, applies a template     |                        |            |
|         | prior that accounts for different search volume as a       |                        |            |
|         | function of intrinsic parameters, and uses an improved     |                        |            |
|         | coherent detection statistic that optimally combines data  |                        |            |
|         | from the Hanford and Livingston detectors. <br>            |                        |            |
|         | [catalog paper](https://arxiv.org/abs/2201.02252)          |                        |            |
+---------+------------------------------------------------------------+------------------------+------------+


## Provenance

| Release  | Provenance                   |
|----------|------------------------------|
| GWTC-2.1 | [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.5759108.svg)](https://doi.org/10.5281/zenodo.5759108)|
| GWTC-3   | [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.5546665.svg)](https://doi.org/10.5281/zenodo.5546665)|
| 4-OGC    | https://github.com/gwastro/4-ogc |
| IAS-O3a  | https://github.com/seth-olsen/new_BBH_mergers_O3a_IAS_pipeline |


## Data journey to the TAP

In this section, we describe the steps that are taken to re-package the original released data into the standard used in the Pipeline Trigger database.

### Download

For each release, a bash script, usually `download.sh`, downloads the original dataset and puts it in the directory `original_data`.

### Preprocessing

The preprocessing step is done by the script `preprocessing.py` and consists in transforming the original data into an hdf5 file written in the directory `converted_data`, whose filename is the catalog name, such as `GWTC-3-MBTA_AllSky.hdf5`. The hdf5 group `original` is used when the catalog contains only one dataset. The types and values of the records in the original catalogs are not altered, with the only exception being when some columns contain values, such as None or other values intended to mean an undefined value. They are set to `NaN` (for floats) or `-999` (for integers) in the hdf5 file of the original and standardized catalogs. These special values are then converted to `null` when they are ingested in the database.


### Conversion to standard

Each pre-processed catalog is then converted the [trigger standard](table_contents.md) by the script `convert.py` and the result is written back in the same hdf5 file, in the hdf5 group `standard`.
