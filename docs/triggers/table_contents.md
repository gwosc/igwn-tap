The standard of the Pipeline Trigger Database, which is a combination of different catalogs, is based on a subset of the GWTC standard for the Candidate Data Release, with minor repackaging changes.

We encourage the curators of pipeline trigger releases, willing to be included in this database to follow this standard to facilitate the integration.

Future versions will contain more fields,  depending on the needs of the scientific community.


<style>
table th:first-of-type {
    width: 20%;
}
table th:nth-of-type(2) {
    width: 5%;
}
table th:nth-of-type(3) {
    width: 5%;
}
table th:nth-of-type(4) {
    width: 70%;
}
</style>

| Standard field  | Type    | Units   | Description |
|-----------------|---------|---------|-------------|
| `name`          | string  |         | Unique identifier of the trigger, prefixed by the catalog name. |
| `original_name` | string  |         | Trigger identifier, as defined in the catalog. Useful to join the original catalog. |
| `release`       | string  |         | Data release name, which may contain more than one catalog (e.g., _GWTC-3_). |
| `catalog`       | string  |         | Catalog name, equal to the release name if there is only one catalog in the release, or prefixed by the release name otherwise (e.g., _GWTC-3-MBTA_AllSky_).|
| `pipeline`      | string  |         | Name of the pipeline used in the analysis, in upper case (e.g., _GSTLAL_, _PYCBC_). |
| `network`       | string  |         | Interferometers, whose data were used to produce the trigger, in alphabetic order (e.g., _H1,L1,V1_). |
| `network_snr`   | float32 |         | Combined Signal-to-Noise ratio for the interferometers in the network. |
| `H1_snr`        | float32 |         | Signal-to-Noise ratio for the Hanford 1 interferometer. |
| `L1_snr`        | float32 |         | Signal-to-Noise ratio for the Livingston 1 interferometer.  |
| `V1_snr`        | float32 |         | Signal-to-Noise ratio for the Virgo 1 interferometer.  |
| `ifar`          | float32 | yr      | Inverse false alarm rate. |
| `end_time_gps`  | float64 | s       | Geo-centric GPS time of the merger. Precise definition may differ, depending on the catalog. |
| `p_astro`       | float32 |         | Bayesian probability of being astrophysical (or not being terrestrial). |
| `mass_1`        | float32 | M<sub>☉</sub> | The detector-frame mass of the heavier object in the binary.   |
| `mass_2`        | float32 | M<sub>☉</sub> | The detector-frame mass of the lighter object in the binary. |
| `spin_1z`       | float32 |               | The z-component of the heavier object's spin parameter in Euclidean coordinates. |
| `spin_2z`       | float32 |               | The z-component of the lighter object's spin parameter in Euclidean coordinates. |
