To inspect the content of the pipeline trigger table:
```sql
SELECT TOP 10 *
FROM triggers.all_triggers
```


To obtain the number of pipeline triggers in the database for each catalog:
```sql
SELECT "catalog", COUNT(*) AS "Number of triggers"
FROM triggers.all_triggers
GROUP BY "catalog"
```
!!! note
    ADQL has reserved keywords, in addition to those reserved by SQL. To use a column identifier that is reserved, it is required to delimit it, i.e. to enclose it with double quote). In this example, the column `catalog` is delimited because it is a SQL reserved keyword. The column alias `Number of triggers` is also delimited because it contains spaces.

    Single quotes are used for string literals.


To know the number of pipeline triggers with an inverse FAR greater than 1 day:
```sql
SELECT COUNT(*)
FROM triggers.all_triggers
WHERE ifar > 1. / 365
```
!!! note
    In SQL `1/365` is zero, so don't forget the dot in `1./365`


It is not possible to define a column alias in the SELECT clause and then use it in the WHERE clause, because ADQL (and ANSI SQL) forbids it. To filter entries based on a calculated column, one has to use a sub-query. For example, to select all the triggers with a total mass greater than 40 M<sub>☉</sub>:
```sql
SELECT name, total_mass
FROM (
    SELECT name, mass_1 + mass_2 AS total_mass
    FROM triggers.all_triggers
)
WHERE total_mass > 40
ORDER BY total_mass DESC
```


To match the pipeline triggers related to an event:
```sql
SELECT
    trig.*
FROM
    triggers.all_triggers AS trig
    INNER JOIN events.all_events AS ev
    ON ABS(trig.end_time_gps - ev.geocent_time_gps) < 1
WHERE
    ev.name = 'GW170817'
```


To cross-match the triggers from all catalogs with an inverse FAR greater than 2 hours, within a time window of 1 second:
```sql
SELECT
    t1.name,
    t2.name,
    ABS(t1.end_time_gps - t2.end_time_gps) AS "Distance"
FROM
    triggers.all_triggers AS t1
    INNER JOIN triggers.all_triggers AS t2
    ON ABS(t1.end_time_gps - t2.end_time_gps) < 1
WHERE
        t1.ifar > 1. / 365 / 12
    AND t2.ifar > 1. / 365 / 12
    AND t1.end_time_gps < t2.end_time_gps
```
!!! notes
    * The request timeout may need to be increased to fulfil the request.
    * To use a column alias that is a reserved keyword, it is required to delimit it, i.e. to enclose it with double quotes. In this example, `distance` is an ADQL reserved keyword.
