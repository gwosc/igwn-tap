# IAS-O3a

## Preprocessing

The data is read from a pandas-written hdf file. The columns `name`, `obs` and `trig` have an object data type, although no None values are present. Their data type is converted to the expected type.
The `ifar` column has one entry equal to zero and many equal to +infinity. These values are kept.


## Standardization

| Standard field  | Derivation from original fields |
|-----------------|---------------------------------|
| `name`          | _IAS-O3a-_ + `name`             |
| `original_name` | `name`                          |
| `release`       | _IAS-O3a_                       |
| `catalog`       | _IAS-O3a'_                      |
| `pipeline`      | _IAS_                           |
| `network`       | _H1,L1_ (see note)              |
| `ifar`          | `ifar`                          |
| `H1_snr`        | `H1_snr`                        |
| `L1_snr`        | `L1_snr`                        |
| `V1_snr`        | _NaN_                           |
| `network_snr`   | sqrt(`H1_snr`² + `L1_snr`²)     |
| `end_time_gps`  | `time` (see note)               |
| `p_astro`       | `pastro`                        |
| `mass_1`        | `mass1`                         |
| `mass_2`        | `mass2`                         |
| `spin_1z`       | `spin1z`                        |
| `spin_2z`       | `spin2z`                        |


!!! note

    `obs` is always equal to 'HLV', but their pipeline performs Hanford-Livingston coincident searches.

!!! note

    From the The dataset keys follow the conventions of the 4-OGC data release,
    except that IAS trigger times are in 'linear-free' coordinates
    (note that these two conventions differ by a shift that depends on
    the intrinsic parameters, with a difference less than 0.1s for most sources
    but up to roughly 0.3s for the lightest BBH systems).
