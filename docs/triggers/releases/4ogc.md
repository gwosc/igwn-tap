# 4-OGC

## Preprocessing

The value of -1 is used in the original catalog to specify a masked value. These values are converted to NaN in the standard table.

## Standardization

| Standard field  | Derivation from original fields |
|-----------------|-----------------------|
| `name`          | _4-OGC-_ + `name`     |
| `original_name` | `name`                |
| `release`       | _4-OGC_               |
| `catalog`       | _4-OGC_               |
| `pipeline`      | _PYCBC_               |
| `network`       | `obs` (_HLV_ → _H1,L1,V1_) |
| `ifar`          | `ifar`                |
| `H1_snr`        | `H1_snr`              |
| `L1_snr`        | `L1_snr`              |
| `V1_snr`        | `V1_snr`              |
| `network_snr`   | sqrt(`H1_snr`² + `L1_snr`² + `V1_snr`²) |
| `end_time_gps`  | `time`                |
| `p_astro`       | `pastro`              |
| `mass_1`        | `mass1`               |
| `mass_2`        | `mass2`               |
| `spin_1z`       | `spin1z`              |
| `spin_2z`       | `spin2z`              |


!!! note

    In the original catalog, the SNRs are cut-off below a value of 4 and set to -1.
    We checked that the interferometers with SNR equal to -1 have been removed from
    the network (`obs` field) and that their event times have also been set to -1.
    So there is no way to know from the original catalog whether the interferometer was
    not observing, or it is was but was not included as an input in the analysis, or if
    it was used in the analysis but it resulted in an SNR <= 4. We cannot exclude that
    some estimated parameters may depend on the strains from sub-threshold interferometers.
