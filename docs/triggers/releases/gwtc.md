# GWTC-2.1 and GWTC-3


## Pre-processing

The GWTC-2.1 and GWTC-3 releases provide information on a per-trigger basis in the form of one xml file per trigger. The first pre-processing step consists for each catalog in stacking the tables `coinc_inspiral` and `sngl_inspiral`. A field `filename` (equal to the xml filename witout the _.xml_ extension) is then added to the two resulting tables, so that additional information (such as P-astro or skymaps), which is stored in other files in the full data releases, can be accessed.

In these releases, values of None or NaN can both be encountered. For float columns, NaN was substituted for None. For integer columns containing None, -999 was substituted. Since the type of the columns of the `astropy.table.Table`s containing a None is set to `object`, the original type may be lost. That's why we had to use the original type mapping from the table definition in the xml files to ensure a consistent column type when stacking the triggers of a given catalog.

The trigger `GWTC-3/H1L1-MBTA_AllSky-1260355124-1` is the only occurence where the interferometers declared in the `coinc_inspiral` field `ifos` does not match the entries in the `sngl_inspiral` table: the `V1` entry is missing in the `sngl_inspiral` table. And only for this trigger, there is a slight mismatch between the combined SNR from the `coinc_inspiral` table and the SNR that can be recomputed from the entries in the `sngl_inspiral` table. It is indicative of a very low `V1` SNR which may explain why the `V1` entry has not made its way into the `sngl_inspiral` table. To keep the mapping consistent between the two tables, we changed the `ifos` value from `H1,L1,V1` to `H1,L1`.

Finally, the two stacked tables are stored in an hdf5 file, with `coinc_inspiral` and `sngl_inspiral` as group names.


## Standardization

<style>
table th:first-of-type {
    width: 20%;
}
table th:nth-of-type(2) {
    width: 80%;
}
</style>

| Standard field  | Derivation from original fields    |
|-----------------|------------------------------------|
| `name`          | from `coinc_inspiral.filename`, prefixed by the catalog name and without the redundant information (_H1L1V1-MBTA_AllSky-1267943416-1_ → _GWTC-3-MBTA_AllSky-H1L1V1-1267943416-1_) |
| `original_name` | `coinc_inspiral.filename`          |
| `release`       | _GWTC-2.1_ or _GWTC-3_               |
| `catalog`       | _GWTC-2.1-GSTLAL_AllSky_, _GWTC-2.1-MBTA_AllSky_, _GWTC-2.1-PYCBC_AllSky_, _GWTC-2.1-PYCBC_HighMass_, _GWTC-3-GSTLAL_AllSky_, _GWTC-3-MBTA_AllSky_, _GWTC-3-PYCBC_AllSky_ or _GWTC-3-PYCBC_HighMass_ |
| `pipeline`      | _GSTLAL_, _MBTA_ or _PYCBC_        |
| `network`       | `coinc_inspiral.ifos`              |
| `network_snr`   | `coinc_inspiral.snr`               |
| `H1_snr`        | `sngl_inspiral[H1].snr`            |
| `L1_snr`        | `sngl_inspiral[L1].snr`            |
| `V1_snr`        | `sngl_inspiral[V1].snr`            |
| `ifar`          | 1 / `coinc_inspiral.combined_far` / 3.1557 10<sup>7</sup>        |
| `end_time_gps`  | `coinc_inspiral.end_time` + `coinc_inspiral.end_time_ns` * 10<sup>-9</sup>|
| `p_astro`       | from file pastro/{`filename`}.json |
| `mass_1`        | `sngl_inspiral[any].mass1`         |
| `mass_2`        | `sngl_inspiral[any].mass2`         |
| `spin_1z`       | `sngl_inspiral[any].spin1z`        |
| `spin_2z`       | `sngl_inspiral[any].spin2z`        |


!!! note

    We have checked that the value of `network_snr` derived from the `coinc_inspiral` table correspond to the L² norm of the per-interferometer `snr` derived from the `sngl_inspiral` table (except for one case, see above).

    We have also checked that the `sngl_inspiral` values of the fields `mass1`, `mass2`, `spin1x`, `spin1y`, `spin2x` and `spin2y` are equal for all the participating interferometers.


!!! note

    Some P-astro files are missing:
    <ul>
        <li> GWTC-3-PYCBC_AllSky/pastro/H1L1-PYCBC_AllSky-1257664691-1.json </li>
        <li> GWTC-3-PYCBC_AllSky/pastro/H1L1-PYCBC_AllSky-1258089700-1.json </li>
        <li> GWTC-3-PYCBC_AllSky/pastro/H1L1-PYCBC_AllSky-1259492747-1.json </li>
        <li> GWTC-3-PYCBC_AllSky/pastro/H1L1-PYCBC_AllSky-1264624567-1.json </li>
        <li> GWTC-3-PYCBC_AllSky/pastro/H1L1-PYCBC_AllSky-1265926102-1.json </li>
        <li> GWTC-3-PYCBC_AllSky/pastro/H1L1-PYCBC_AllSky-1266055539-1.json </li>
        <li> GWTC-3-PYCBC_AllSky/pastro/H1L1-PYCBC_AllSky-1267522652-1.json </li>
        <li> GWTC-3-PYCBC_AllSky/pastro/H1L1-PYCBC_AllSky-1267724187-1.json </li>
        <li> GWTC-3-PYCBC_AllSky/pastro/H1L1-PYCBC_AllSky-1268903511-1.json </li>
        <li> GWTC-3-PYCBC_HighMass/pastro/H1L1-PYCBC_HighMass-1257664691-1.json </li>
        <li> GWTC-3-PYCBC_HighMass/pastro/H1V1-PYCBC_HighMass-1263611886-1.json </li>
    </ul>
