Inspect the event table
```sql
SELECT *
FROM events.all_events
```

Select an event by name:
```sql
SELECT *
FROM events.all_events
WHERE name = 'GW170817'
```

Select an event by GPS time:
```sql
SELECT *
FROM events.all_events
WHERE ABS(geocent_time_gps - 1187008882) < 1
```

More complex queries can be constructed. The following query computes the average redshift of the detected compact binaries per observing run. We can see that the horizon increases as the sensitivity of the interferometers increases.
```sql
WITH subquery AS (
    SELECT
        redshift,
        CASE
            WHEN geocent_time_gps < 1164556817 THEN 'O1'  -- 30th Nov 2016 16:00 UTC
            WHEN geocent_time_gps < 1238166018 THEN 'O2'  -- 1st April 2019 15:00 UTC
            ELSE 'O3'
        END AS observing_run
    FROM events.all_events
    WHERE redshift IS NOT NULL
)
SELECT observing_run, AVG(redshift) AS "Average redshift"
FROM subquery
GROUP BY observing_run
ORDER BY observing_run
```
