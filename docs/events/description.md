The IGWN TAP contains gravitational wave events, accessible in the table `events.all_events`. These events are drawn from the list of [all available events](https://www.gw-openscience.org/eventapi/html/allevents), which includes the confident and marginal detections for the following releases:

| Release  | Reference | Description   |
|----------|-----------|---------------|
| GWTC-1   | [link](https://www.gw-openscience.org/GWTC-1)   | A Gravitational-Wave Transient Catalog of Compact Binary Mergers Observed by LIGO and Virgo during the First and Second Observing Runs (2015 — 2017). |
| GWTC-2.1 | [link](https://www.gw-openscience.org/GWTC-2.1) | Deep Extended Catalog of Compact Binary Coalescences Observered by LIGO and VIRGO in the First Half of the Third Observing Run (April 2019 — September 2019). This release contains an additional catalog (`GWTC-2.1-auxiliary`) for the events included in the GWTC-2 release that have been demoted after re-analysis. |
| GWTC-3   | [link](https://www.gw-openscience.org/GWTC-3)   | Compact Binary Coalescences Observed by LIGO and Virgo During the Second Part of the Third Observing Run (November 2019 — March 2020). |
| O3-IMBH-marginal | [link](https://www.gw-openscience.org/O3_IMBH_marginal) | This release includes data associated with marginal intermediate-mass black hole candidates in O3, initially published outside of main catalogs and O3 Discovery Papers. |


!!! note
    For each event, only the latest version of the processing is kept in the table `events.all_events`.
