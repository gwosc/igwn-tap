Few fields in the original JSON file have been renamed, in order to increase the compatibility with the pipeline trigger table.

| Name                  | Name in JSON | Units | Description |
|-----------------------|--------------|-------|-------------|
| `name`                | `commonName` |       | The unique identifier of the event. |
| `release`             | ∅            |       | The data release in which the event has been published (e.g., *GWTC-2.1*). |
| `catalog`             | `catalog. shortName`|| The data release catalog that contains the event (e.g., *GWTC-2.1-confident*). |
| `network_matched_ filter_snr` |      |       | The matched filter signal to noise ratio in the gravitational wave detector network. |
| `far`                 |              | yr⁻¹  | The false alarm rate. |
| `ifar`                | ∅            | yr    | The inverse false alarm rate. |
| `geocent_time_gps`    | `GPS`        | s     | The GPS merger time at the geocenter. |
| `p_astro`             |              |       | The bayesian probability of being astrophysical (or not being terrestrial). |
| `luminosity_distance` |              | Mpc   | The luminosity distance of the source. |
| `redshift`            |              |       | The redshift depending on specified cosmology. |
| `mass_1_source`       |              | M<sub>☉</sub> | The source-frame mass of the heavier object in the binary. |
| `mass_2_source`       |              | M<sub>☉</sub> | The source-frame mass of the lighter object in the binary. |
| `total_mass_source`   |              | M<sub>☉</sub> | The source-frame combined mass of the primary and secondary masses. |
| `final_mass_source`   |              | M<sub>☉</sub> | The source-frame remnant mass estimated using the spins evolved to the ISCO frequency. |
| `chirp_mass`          |              | M<sub>☉</sub> | The detector-frame chirp mass. |
| `chirp_mass_source`   |              | M<sub>☉</sub> | The source-frame chirp mass. |
| `chi_eff`             |              |       | The effective inspiral spin parameter. |
| `jsonurl`             |              |       | The URL of the JSON data. |
| `reference`           |              |       | The reference identifying the data release. |
| `version`             |              |       | The version of the event entry. |

!!! note
    The lower and upper values of the estimated parameters are not yet in the table. We are interacting with VO specialists to properly include them.
