The detailed procedure to install the DaCHS software is described in this [document](https://dachs-doc.readthedocs.io/install.html). Here, we detail the specific steps required for the IGWN TAP instalation and customization on a debian-based distribution.

| Environment | Server                   | User   | Distribution    |
|-------------|--------------------------|--------|-----------------|
| production  | ccwbvps22.in2p3.fr       | ubuntu | Ubuntu 22.04    |
| staging     | vps-fe1543c2.vps.ovh.net | debian | Debian/bullseye |


## Setup

First, the GAVO key should be imported
```bash
sudo curl https://docs.g-vo.org/archive-key.asc | sudo tee gavo-archive.key.asc
```

and the stable release repository be added to the file `/etc/apt/sources.list`.
```
deb http://vo.ari.uni-heidelberg.de/debian release main
```

The DaCHS software is already packaged and is straightforward to install
```bash
sudo apt install --yes gavodachs2-server
```


## Customization

Unless specified otherwise, the commands are assumed to be run from a computer that has access to the in2p3 network and which has been authorized to connect to the TAP server. `igwn-tap` is an ssh alias for the TAP server.

Customize the configuration file
```
scp ansible/files/gavo.rc igwn-tap:
ssh igwn-tap 'sudo chown dachsroot:gavo gavo.rc && sudo mv gavo.rc /etc/'
```
On the remote server, fill in the admin password in the file /etc/gavo.rc.


Customize logo
```
curl -O https://www.gw-openscience.org/static/images/ligo_virgo_kagra_blue2orange_light.png
sudo mkdir /var/gavo/web/nv_static/img
sudo chown dachsroot:gavo /var/gavo/web/nv_static/img
sudo mv ligo_virgo_kagra_blue2orange_light.png /var/gavo/web/nv_static/img/
```

Customize the default metadata
```
scp ansible/files/defaultmeta.txt igwn-tap:
ssh igwn-tap 'sudo chown dachsroot:gavo defaultmeta.txt && sudo mv defaultmeta.txt /var/gavo/etc/'
```

Customize the TAP query form
```
ssh igwn-tap 'sudo mkdir -p /var/gavo/inputs/adqlform && sudo chown dachsroot:gavo /var/gavo/inputs/adqlform'
scp ansible/files/adqlform.rd igwn-tap:
ssh igwn-tap 'sudo chown dachsroot:gavo adqlform.rd && sudo mv adqlform.rd /var/gavo/inputs/adqlform/q'
ssh igwn-tap 'sudo dachs pub /var/gavo/inputs/adqlform/q'
```

And finally, to restart the DaCHS server
```
ssh igwn-tap 'sudo systemctl restart dachs'
```


!!! note
    A [project](https://github.com/gavodachs/ansible-dachs) exists to automate the installation and customization of DaCHS using Ansible. We have not used it, but it could be a good idea to take the time to evaluate it.

!!! note
    I found it useful to check the site from the remote VM, running:
    ```bash
    sudo docker run --network host -it browsh/browsh
    ```
