The event and trigger database are described by Resource Descriptor files. The description includes table and column annotations through metadata tags, but also the recipe to ingest the tables from a source data file (here CSV files).

Before deploying these RDs, it is necessary to build the CSV files locally.

After running `poetry shell`, for the event table:
```bash
events/download.sh
python events/convert.py
```

For the trigger table:
```bash
triggers/download.sh
triggers/preprocess_and_convert.sh
python triggers/write_csvs.py
```

The deployment of the RDs together with their associated data is automated using ansible. The following commands should be run from a computer in the in2p3 network that is authorized to connect to the TAP server.

To re-deploy the event database (Resource Descriptor and table data) on the staging server:
```shell
ansible-playbook -v -i ansible/hosts_staging ansible/deploy_events.yml
```

To re-deploy the trigger database (Resource Descriptor and table data) on the production server:
```shell
ansible-playbook -v -i ansible/hosts_production ansible/deploy_triggers.yml
```