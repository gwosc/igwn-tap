
The IGWN TAP server is a deployment of the [DaCHS](https://dachs-doc.readthedocs.io) software stack. It provides a Python-based all-battery included server that can manage various VO services, including a TAP service.


## Table annotations for the VO tools

The Pipeline Trigger Table is annotated for interoperability with the VO tools.

* the units of the tables follow the IVOA Working Draft 2022-05-25 Units in the VO [Version 1.1](https://ivoa.net/documents/VOUnits/20220525/WD-VOUnits-1.1-20220525.pdf).

* the Unified Content Descriptions, which describe the physical meaning of the columns follow the IVOA Standard for Unified Content Descriptors
[Version 1.10](https://www.ivoa.net/documents/latest/UCD.html). The [IVAO UCD builder](http://cdsweb.u-strasbg.fr/UCD/cgi-bin/descr2ucd) was handy to find the UCD corresponding to a description in natural language.

!!! note
    Some quantities used in the event or the trigger databases, such as the ones specific to the CBC science, had not been standardized yet, This is the list of UCDs that will be proposed to the IVOA standardization commitee.

    | UCD                  | Description                                   |
    |----------------------|-----------------------------------------------|
    | stat.falsePositive   | Related to false alarm or false positive detection. |
    | stat.falseNegative   | Related to missed or false negative detection. |
    | phys.spinParameter   | Related to the dimensionless compact object’s spin parameter. |
    | phys.inspiralSpinParameter | Related to the dimensionless compact binary inspiral spin parameter. |

## Column indexing

The following table describes the table columns that are indexed

| Table        | Column names           |
|--------------|------------------------|
| all_events   | name, geocent_time_gps |
| all_triggers | catalog, end_time_gps  |

As we gain understanding on how the Pipeline Trigger Database is used, we may index more columns.
