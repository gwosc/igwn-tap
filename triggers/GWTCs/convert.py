import json
from pathlib import Path
from typing import Iterable

import numpy as np
from astropy.table import Table
from numpy.testing import assert_allclose

ORIGINAL_DATA_PATH = Path(__file__).parent / 'original_data'
CONVERTED_DATA_PATH = Path(__file__).parent / 'converted_data'

NAN32 = np.array(np.nan, np.float32)
IFOS = 'H1', 'L1', 'V1'

def get_name(name_from_file: str, short_catalog_name: str) -> str:
    short_name = name_from_file.replace(f'-{short_catalog_name}', '')
    return f'{catalog}-{short_name}'


def collect(coinc: Table, sngl: Table, fields: list[str]) -> dict[str, dict[str, list]]:
    ifos = {ifo: {field: [] for field in fields} for ifo in IFOS}
    for trigger in coinc:
        filename = trigger['filename']
        for ifo in 'H1', 'L1', 'V1':
            match = sngl[(sngl['filename'] == filename) & (sngl['ifo'] == ifo)]
            nmatch = len(match)
            for field in fields:
                if nmatch == 0:
                    ifos[ifo][field].append(NAN32)
                elif nmatch == 1:
                    ifos[ifo][field].append(match[0][field])
                else:
                    raise
    return ifos


def get_p_astro(filename) -> np.number:
    pastro_path = ORIGINAL_DATA_PATH / catalog / 'pastro' / (filename + '.json')
    if not pastro_path.exists():
        print(f'Could not find: {pastro_path}')
        return NAN32
    pastro_content = json.loads(pastro_path.read_text())
    return pastro_content['Astro']


def convert(catalog) -> Table:
    release, short_catalog_name = catalog.rsplit('-', 1)
    print(f'Converting {release}/{short_catalog_name}')
    pipeline = short_catalog_name.split('_')[0]

    coinc = Table.read(CONVERTED_DATA_PATH / (catalog + '.hdf5'), path='coinc_inspiral')
    sngl = Table.read(CONVERTED_DATA_PATH / (catalog + '.hdf5'), path='sngl_inspiral')

    output = Table()
    output['name'] = [get_name(_, short_catalog_name) for _ in coinc['filename']]
    output['original_name'] = coinc['filename']
    output['release'] = release
    output['catalog'] = catalog
    output['pipeline'] = pipeline
    output['network'] = coinc['ifos']
    output['network_snr'] = coinc['snr'].astype(np.float32)
    ifos = collect(coinc, sngl, 'snr,mass1,mass2,spin1x,spin1y,spin1z,spin2x,spin2y,spin2z'.split(','))
    output['H1_snr'] = ifos['H1']['snr']
    output['L1_snr'] = ifos['L1']['snr']
    output['V1_snr'] = ifos['V1']['snr']

    mask = coinc['filename'] != 'H1L1-MBTA_AllSky-1260355124-1' # unique mismatch ifos in GWTC3
    expected_snr = np.sqrt(
        np.nansum(
            np.array([output['H1_snr'], output['L1_snr'], output['V1_snr']])**2,
            axis=0
        )
    )
    assert_allclose(output['network_snr'][mask], expected_snr[mask], atol=2e-6)
    output['ifar'] = (1 / coinc['combined_far'] / 3.1557e+7).astype(np.float32)
    output['end_time_gps'] = coinc['end_time'] + 1e-9 * coinc['end_time_ns']

    output['p_astro'] = np.array([get_p_astro(_) for _ in coinc['filename']], np.float32)

    # we check that the values in the sngl_inspiral table are the same for all the participating ifos
    for outfield in 'mass_1,mass_2,spin_1z,spin_2z'.split(','):
        field = outfield.replace('_', '')
        var = np.nanvar([ifos['H1'][field], ifos['L1'][field], ifos['V1'][field]], axis=0)
        assert_allclose(var, 0, atol=1e-9)
        output[outfield] = np.nanmean([ifos['H1'][field], ifos['L1'][field], ifos['V1'][field]], axis=0)

    return output


for catalog_path in sorted(ORIGINAL_DATA_PATH.iterdir()):
    catalog = catalog_path.name
    if 'CWB' in catalog:
        continue
    output = convert(catalog)
    table_path = CONVERTED_DATA_PATH / f'{catalog}.hdf5'
    output.write(table_path, path='standard', overwrite=True, append=True)


# Could not find: GWTCs/original_data/GWTC-3-PYCBC_AllSky/pastro/H1L1-PYCBC_AllSky-1257664691-1.json
# Could not find: GWTCs/original_data/GWTC-3-PYCBC_AllSky/pastro/H1L1-PYCBC_AllSky-1258089700-1.json
# Could not find: GWTCs/original_data/GWTC-3-PYCBC_AllSky/pastro/H1L1-PYCBC_AllSky-1259492747-1.json
# Could not find: GWTCs/original_data/GWTC-3-PYCBC_AllSky/pastro/H1L1-PYCBC_AllSky-1264624567-1.json
# Could not find: GWTCs/original_data/GWTC-3-PYCBC_AllSky/pastro/H1L1-PYCBC_AllSky-1265926102-1.json
# Could not find: GWTCs/original_data/GWTC-3-PYCBC_AllSky/pastro/H1L1-PYCBC_AllSky-1266055539-1.json
# Could not find: GWTCs/original_data/GWTC-3-PYCBC_AllSky/pastro/H1L1-PYCBC_AllSky-1267522652-1.json
# Could not find: GWTCs/original_data/GWTC-3-PYCBC_AllSky/pastro/H1L1-PYCBC_AllSky-1267724187-1.json
# Could not find: GWTCs/original_data/GWTC-3-PYCBC_AllSky/pastro/H1L1-PYCBC_AllSky-1268903511-1.json
# Could not find: GWTCs/original_data/GWTC-3-PYCBC_HighMass/pastro/H1L1-PYCBC_HighMass-1257664691-1.json
# Could not find: GWTCs/original_data/GWTC-3-PYCBC_HighMass/pastro/H1V1-PYCBC_HighMass-1263611886-1.json
