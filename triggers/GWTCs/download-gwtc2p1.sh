# -- GWTC 2.1 Candidate data release
set -ex

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

wget https://zenodo.org/record/5759108/files/search_data_GWTC2p1.tar.gz?download=1 -O data-gwtc2p1.tar.gz
tar -xf data-gwtc2p1.tar.gz
rm data-gwtc2p1.tar.gz

OUT_DIR=${SCRIPT_DIR}/original_data
mkdir -p ${OUT_DIR}
mv search_data_products/* ${OUT_DIR}
rm -r search_data_products
mv ${OUT_DIR}/gstlal_all_sky ${OUT_DIR}/GWTC-2.1-GSTLAL_AllSky
mv ${OUT_DIR}/mbta_all_sky ${OUT_DIR}/GWTC-2.1-MBTA_AllSky
mv ${OUT_DIR}/pycbc_all_sky ${OUT_DIR}/GWTC-2.1-PYCBC_AllSky
mv ${OUT_DIR}/pycbc_highmass ${OUT_DIR}/GWTC-2.1-PYCBC_HighMass
