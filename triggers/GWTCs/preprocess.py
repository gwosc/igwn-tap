from pathlib import Path

from gwpy.table import Table
from astropy.table import vstack


ORIGINAL_DATA_PATH = Path(__file__).parent / 'original_data'
CONVERTED_DATA_PATH = Path(__file__).parent / 'converted_data'
CONVERTED_DATA_PATH.mkdir(exist_ok=True)

DATA_TYPES = {
    'coinc_inspiral': {
        'coinc_event:coinc_event_id': '<i8',
        'combined_far': '<f8',
        'end_time': '<i4',
        'end_time_ns': '<i4',
        'false_alarm_rate': '<f8',
        'ifos': str,
        'mass': '<f8',
        'mchirp': '<f8',
        'minimum_duration': '<f8',
        'snr': '<f8',
    },
    'sngl_inspiral': {
            'Gamma0': '<f4',
            'Gamma1': '<f4',
            'Gamma2': '<f4',
            'Gamma3': '<f4',
            'Gamma4': '<f4',
            'Gamma5': '<f4',
            'Gamma6': '<f4',
            'Gamma7': '<f4',
            'Gamma8': '<f4',
            'Gamma9': '<f4',
            'alpha': '<f4',
            'alpha1': '<f4',
            'alpha2': '<f4',
            'alpha3': '<f4',
            'alpha4': '<f4',
            'alpha5': '<f4',
            'alpha6': '<f4',
            'amplitude': '<f4',
            'bank_chisq': '<f4',
            'bank_chisq_dof': '<i4',
            'beta': '<f4',
            'channel': 'S34',
            'chi': '<f4',
            'chisq': '<f8',
            'chisq_dof': '<i4',
            'coa_phase': '<f4',
            'cont_chisq': '<f4',
            'cont_chisq_dof': '<i4',
            'eff_distance': '<f4',
            'end_time': '<i4',
            'end_time_gmst': '<f8',
            'end_time_ns': '<i4',
            'eta': '<f4',
            'event_duration': '<f8',
            'event_id': '<i8',
            'f_final': '<f4',
            'ifo': 'S2',
            'impulse_time': '<i4',
            'impulse_time_ns': '<i4',
            'kappa': '<f4',
            'mass1': '<f4',
            'mass2': '<f4',
            'mchirp': '<f4',
            'mtotal': '<f4',
            'process_id': '<i8',
            'psi0': '<f4',
            'psi3': '<f4',
            'rsqveto_duration': '<f4',
            'search': str,
            'sigmasq': '<f8',
            'snr': '<f4',
            'spin1x': '<f4',
            'spin1y': '<f4',
            'spin1z': '<f4',
            'spin2x': '<f4',
            'spin2y': '<f4',
            'spin2z': '<f4',
            'tau0': '<f4',
            'tau2': '<f4',
            'tau3': '<f4',
            'tau4': '<f4',
            'tau5': '<f4',
            'template_duration': '<f8',
            'ttotal': '<f4',
}
}


def fix_dtype_object(table: Table, table_name: str):
    for colname in table.colnames:
        if colname.endswith('_id'):
            if table[colname].dtype.kind == 'i':
                continue
            table[colname] = [int(_) for _ in table[colname]]

        elif table[colname].dtype.kind == 'O':
            expected_type = DATA_TYPES[table_name][colname]
            if 'i' in expected_type:
                print(f'Setting -999 for None in integer column {table_name}.{colname}')
                table[colname][table[colname] == None] = -999
            table[colname] = table[colname].astype(expected_type)


def read_trigger(trigger: Path, table_name: str, catalog: str) -> list[Table]:
    table = Table.read(trigger, tablename=table_name, use_numpy_dtypes=False)
    fix_dtype_object(table, table_name)
    table['filename'] = trigger.stem.removesuffix('.xml')
    return table


def read_gwtc2p1_gwtc3_triggers(path: Path, catalog: str) -> dict[str, Table]:
    triggers = sorted(path.glob('*.xml*'))
    print(f'Found {len(triggers)} triggers in {path}')

    coincs = []
    sngls = []

    for trigger in triggers:
        coinc = read_trigger(trigger, 'coinc_inspiral', catalog)
        coincs.append(coinc)
        sngl = read_trigger(trigger, 'sngl_inspiral', catalog)
        sngls.append(sngl)
        actual_ifos = ','.join(sorted(_['ifo'] for _ in sngl))
        expected_ifos = coinc[0]['ifos']
        if actual_ifos != expected_ifos:
            print(f'Fixing IFO mismatch for {trigger}: {actual_ifos} != {expected_ifos}')
            coinc[0]['ifos'] = actual_ifos

    tables = {
        'coinc_inspiral': vstack(coincs),
        'sngl_inspiral': vstack(sngls),
    }

    return tables


pipeline_paths = sorted(ORIGINAL_DATA_PATH.iterdir())

for catalog_path in pipeline_paths:
    catalog = catalog_path.name
    if 'CWB' in catalog:
        continue
    tables = read_gwtc2p1_gwtc3_triggers(catalog_path, catalog)
    table_path = CONVERTED_DATA_PATH / f'{catalog}.hdf5'
    table_path.unlink(missing_ok=True)
    for table_name, table in tables.items():
        table.write(table_path, path=table_name, append=True)
