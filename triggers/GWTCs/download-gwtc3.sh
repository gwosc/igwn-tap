# -- GWTC-3 Candidate data release
set -ex

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

wget https://zenodo.org/record/5546665/files/search_data.tar.gz?download=1 -O data-gwtc3.tar.gz
tar -xf data-gwtc3.tar.gz
rm data-gwtc3.tar.gz

OUT_DIR=${SCRIPT_DIR}/original_data
mkdir -p ${OUT_DIR}
mv search_data_products/* ${OUT_DIR}
rm -r search_data_products
mv ${OUT_DIR}/cwb_allsky ${OUT_DIR}/GWTC-3-CWB_AllSky
mv ${OUT_DIR}/gstlal_allsky ${OUT_DIR}/GWTC-3-GSTLAL_AllSky
mv ${OUT_DIR}/mbta_all_sky ${OUT_DIR}/GWTC-3-MBTA_AllSky
mv ${OUT_DIR}/pycbc_all_sky ${OUT_DIR}/GWTC-3-PYCBC_AllSky
mv ${OUT_DIR}/pycbc_highmass ${OUT_DIR}/GWTC-3-PYCBC_HighMass
