set -ex

for dir in 4-OGC GWTCs IAS-O3a
do
    python $dir/preprocess.py
    python $dir/convert.py
done
