set -ex

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

wget -P ${SCRIPT_DIR}/original_data https://github.com/seth-olsen/new_BBH_mergers_O3a_IAS_pipeline/raw/main/IAS_O3a_triggers.hdf
