from pathlib import Path

import numpy as np
from astropy.table import Table

CATALOG = 'IAS-O3a'
CONVERTED_DATA_PATH = Path(__file__).parent / 'converted_data' / f'{CATALOG}.hdf5'
NAN32 = np.array(np.nan, np.float32)

input = Table.read(CONVERTED_DATA_PATH, path='original')

output = Table()
output['name'] = [f'{CATALOG}-{_}' for _ in input['name']]
output['original_name'] = input['name']
output['release'] = CATALOG
output['catalog'] = CATALOG
output['pipeline'] = 'IAS'
network = set(input['obs'])
assert network == {'HLV'}
output['network'] = 'H1,L1'
network_snr = np.sqrt(input['H1_snr']**2 + input['L1_snr']**2)
output['network_snr'] = network_snr.astype(np.float32)
output['H1_snr'] = input['H1_snr'].astype(np.float32)
output['L1_snr'] = input['L1_snr'].astype(np.float32)
output['V1_snr'] = NAN32
output['ifar'] = input['ifar'].astype(np.float32)
output['end_time_gps'] = input['time']
output['p_astro'] = input['pastro'].astype(np.float32)
output['mass_1'] = input['mass1'].astype(np.float32)
output['mass_2'] = input['mass2'].astype(np.float32)
output['spin_1z'] = input['spin1z'].astype(np.float32)
output['spin_2z'] = input['spin2z'].astype(np.float32)

output.write(CONVERTED_DATA_PATH, path='standard', append=True, overwrite=True)
