from pathlib import Path

import numpy as np
import pandas as pd
from astropy.table import Table


def assert_no_none(colname: str) -> None:
    assert all(_ is not None for _ in input[colname])

CATALOG = 'IAS-O3a'
ORIGINAL_DATA_PATH = Path(__file__).parent / 'original_data' / 'IAS_O3a_triggers.hdf'
CONVERTED_DATA_PATH = Path(__file__).parent / 'converted_data' / f'{CATALOG}.hdf5'
CONVERTED_DATA_PATH.parent.mkdir(exist_ok=True)

input_pandas = pd.read_hdf(ORIGINAL_DATA_PATH)
input = Table()
for key in input_pandas.columns:
    input[key] = input_pandas[key]

# These columns have a 'O' type, but they do not contain None. We fix them.
for colname in ['name', 'obs', 'trig']:
    assert_no_none(colname)
    input[colname] = [_ for _ in input[colname]]


# There is one instance of ifar=0. Many more with ifar=inf. We keep all of them.
# All columns are ok
for colname in input.colnames:
    column = input[colname]
    if column.dtype != float:
        continue
    if np.sum(np.isnan(column)) != 0:
        print('NaN:', column.name)
    if np.sum(np.isneginf(column)) != 0:
        print('-Inf:', column.name)
    if np.sum(np.isposinf(column)) != 0:
        print('+Inf:', column.name)
    if n := np.sum(column == -1) != 0 and column.name not in {'spin1z', 'spin2z'}:
        print('-1:', column.name)

# All columns looks good
for colname in input.colnames:
    column = input[colname]
    if column.dtype != float:
        continue
    print(column.name, np.min(column), np.max(column))

if CONVERTED_DATA_PATH.exists():
    CONVERTED_DATA_PATH.unlink()
input.write(CONVERTED_DATA_PATH, path='original')


# Output: 
# +Inf: ifar
# H1_snr 4.002706979856141 11.92978398142172
# L1_snr 4.002706979856141 11.92978398142172
# ifar 0.0 inf
# pastro 0.0 0.9999999725999121
# time 1238176047.709961 1253973623.458252
# mass1 2.9579180928933284 200.0
# mass2 1.608317518095815 100.0
# spin1z -1.0 1.0
# spin2z -1.0 1.0
# stat -100023.53048466958 44.08965784882942
#
# No nan
