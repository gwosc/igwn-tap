from pathlib import Path

import numpy as np
from astropy.table import Table
from numpy.testing import assert_allclose

CATALOG = '4-OGC'
CONVERTED_DATA_PATH = Path(__file__).parent / 'converted_data' / f'{CATALOG}.hdf5'

input = Table.read(CONVERTED_DATA_PATH, path='original')

output = Table()
output['name'] = [f'{CATALOG}-{_}' for _ in input['name']]
output['original_name'] = input['name']
output['release'] = CATALOG
output['catalog'] = CATALOG
output['pipeline'] = 'PYCBC'
output['network'] = [','.join(sorted(f'{_}1' for _ in obs)) for obs in input['obs']]
assert all(len(_) > 0 for _ in output['network'])

H1_snr = input['H1_snr']
L1_snr = input['L1_snr']
V1_snr = input['V1_snr']
assert np.all(~np.isfinite(H1_snr) | (H1_snr > 4))
assert np.all(~np.isfinite(L1_snr) | (L1_snr > 4))
assert np.all(~np.isfinite(V1_snr) | (V1_snr > 4))
network_snr = np.sqrt(
    np.nansum(
        np.array([H1_snr, L1_snr, V1_snr])**2,
        axis=0
    )
)
assert np.all(network_snr > 0)
output['network_snr'] = network_snr.astype(np.float32)
output['H1_snr'] = H1_snr.astype(np.float32)
output['L1_snr'] = L1_snr.astype(np.float32)
output['V1_snr'] = V1_snr.astype(np.float32)
output['ifar'] = input['ifar'].astype(np.float32)

time_H1 = input['H1_end_time']
time_L1 = input['L1_end_time']
time_V1 = input['V1_end_time']
output['end_time_gps'] = input['time']
assert np.all(np.isfinite(output['end_time_gps']))
assert_allclose(output['end_time_gps'], np.nanmean([time_H1, time_L1, time_V1], axis=0), rtol=1e-15)

output['p_astro'] = input['pastro']
output['mass_1'] = input['mass1']
output['mass_2'] = input['mass2']
output['spin_1z'] = input['spin1z']
output['spin_2z'] = input['spin2z']

output.write(CONVERTED_DATA_PATH, path='standard', append=True, overwrite=True)
