from pathlib import Path

import h5py
import numpy as np
from astropy.table import Table

CATALOG = '4-OGC'
ORIGINAL_DATA_PATH = Path(__file__).parent / 'original_data' / '4-OGC_small.hdf'
CONVERTED_DATA_PATH = Path(__file__).parent / 'converted_data' / f'{CATALOG}.hdf5'
CONVERTED_DATA_PATH.parent.mkdir(exist_ok=True)

input = Table()
with h5py.File(ORIGINAL_DATA_PATH) as triggers:
    for key in triggers:
        input[key] = triggers[key][:]

# Many columns with -1 values
for colname in input.colnames:
    column = input[colname]
    if column.dtype.kind != 'f':
        continue
    if n := np.sum(np.isnan(column)):
        print('# NaN:', column.name, n)
        assert n == 0
    if n := np.sum(np.isneginf(column)):
        print('# -Inf:', column.name, n)
        assert n == 0
    if n := np.sum(np.isposinf(column)):
        print('# +Inf:', column.name, n)
        assert n == 0
    if n := np.sum(column == -1):
        print('# Fixing -1:', column.name, n)
        input[colname][column == -1] = np.nan

# All columns looks good now
for colname in input.colnames:
    column = input[colname]
    if column.dtype.kind != 'f':
        continue
    print('#', column.name, np.nanmin(column), np.nanmax(column))

print(
    '# H1 in network, but H1_snr <= 4: ',
    np.sum(np.array(['H' in _ for _ in input['obs']]) & (input['H1_snr'] <= 4))
)
print(
    '# L1 in network, but L1_snr <= 4: ',
    np.sum(np.array(['L' in _ for _ in input['obs']]) & (input['L1_snr'] <= 4))
)
print(
    '# V1 in network, but V1_snr <= 4: ',
    np.sum(np.array(['V' in _ for _ in input['obs']]) & (input['V1_snr'] <= 4))
)
# All zeros:
#    In the original catalog, the SNRs are cut-off below a value of 4 and set to -1.
#    We checked that the interferometers with SNR equal to -1 have been removed from
#    the network (`obs` field) and that their event times have also been set to -1.
#    So there is no way to know from the original catalog whether the interferometer was
#    not observing, or it is was but was not included as an input in the analysis, or if
#    it was used in the analysis but it resulted in an SNR <= 4. We cannot exclude that
#    some estimated parameters may depend on the strains from sub-threshold interferometers.

if CONVERTED_DATA_PATH.exists():
    CONVERTED_DATA_PATH.unlink()
input.write(CONVERTED_DATA_PATH, path='original')


# Output:
# Fixing -1: H1_chisq 283943
# Fixing -1: H1_end_time 283943
# Fixing -1: H1_sg_chisq 283943
# Fixing -1: H1_snr 283943
# Fixing -1: L1_chisq 252965
# Fixing -1: L1_end_time 252965
# Fixing -1: L1_sg_chisq 252965
# Fixing -1: L1_snr 252965
# Fixing -1: V1_chisq 887851
# Fixing -1: V1_end_time 887851
# Fixing -1: V1_sg_chisq 887851
# Fixing -1: V1_snr 887851
# Fixing -1: ifar 8
# Fixing -1: pastro 1424837
# H1_chisq 0.0 43.54441
# H1_end_time 1126073558.9926758 1269296071.340332
# H1_sg_chisq 0.061649453 7.5313354
# H1_snr 4.000000476837158 19.934932708740234
# L1_chisq 0.0 38.404854
# L1_end_time 1126073558.9863281 1269296071.3378906
# L1_sg_chisq 0.0729806 3.9504662
# L1_snr 4.000044345855713 25.532669067382812
# V1_chisq -0.0 25.8382
# V1_end_time 1185617354.8671875 1269285810.019043
# V1_sg_chisq 0.046047926 3.88985
# V1_snr 4.000069618225098 13.135703086853027
# ifar 1.0000001e-06 32570.703
# mass1 1.0000006 492.41434
# mass2 1.0000035 499.67432
# pastro 0.2 1.0
# spin1z -0.94999653 0.9499993
# spin2z -0.9499994 0.9499996
# stat -30.0 177.9925
# time 1126073558.989502 1269296071.3391113
# H1 in network, but H1_snr <= 4:  0
# L1 in network, but L1_snr <= 4:  0
# V1 in network, but V1_snr <= 4:  0
