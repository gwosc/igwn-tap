set -ex

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

wget -P ${SCRIPT_DIR}/original_data https://github.com/gwastro/4-ogc/raw/master/search/4-OGC_small.hdf
