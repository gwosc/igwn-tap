from pathlib import Path

from astropy.table import Table, vstack

catalogs = sorted(Path(__file__).parent.glob('*/converted_data/*hdf5'))

tables = [Table.read(catalog, path='standard') for catalog in catalogs]
table: Table = vstack(tables)
df = table.to_pandas(index=False)
for key in ['name', 'original_name', 'release', 'catalog', 'pipeline', 'network']:
    df[key] = [_.decode() for _ in df[key]]
df.to_csv('all_triggers.csv.gz', na_rep='', index=False, compression='gzip')

