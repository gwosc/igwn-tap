# IGWN TAP

TAP access to gravitational wave event catalogs and pipeline trigger catalogs from the International Gravitational Wave Network, curated by the GWOSC or by non-LVK external collaborations. The documentation is [here](https://gwosc.docs.ligo.org/igwn-tap/)
