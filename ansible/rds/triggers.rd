<resource schema="triggers">
  <meta name="title">Pipeline Trigger Database</meta>
  <meta name="description">
    Pipeline triggers.
  </meta>
  <meta name="creationDate">2022-09-08T12:55:00</meta>
  <meta name="subject">gravitational waves</meta>
  <meta name="creator">Gravitation Wave Open Science Center</meta>
  <meta name="instrument">L1, H1, V1</meta>
  <meta name="facility">LIGO/VIRGO</meta>
  <meta name="source">https://gwosc.docs.ligo.org/igwn-tap</meta>
  <meta name="contentLevel">Research</meta>
  <meta name="type">Catalog</meta>

  <table>
    <column id="column_name" name="name" type="text"
      ucd="meta.id;meta.main" tablehead="Name" verbLevel="1"
      description="The unique identifier of the trigger, prefixed by the catalog name."
    />
    <column id="column_original_name" name="original_name" type="text"
      ucd="meta.id" tablehead="Original Name" verbLevel="1"
      description="The trigger identifier, as defined in the catalog. Useful to join the original catalog."
    />
    <column id="column_release" name="release" type="text"
      ucd="meta.id" tablehead="Release" verbLevel="1"
      description="The release name, which may contain more than one catalog (ex: GWTC-3)."
    />
    <column id="column_catalog" name="quoted/catalog" type="text"
      ucd="meta.id;meta.table" tablehead="Catalog" verbLevel="1"
      description="The catalog name, equal to the release name if there is only one catalog in the release, or prefixed by the release name otherwise (ex: GWTC-3-MBTA_AllSky)."
    />
    <column id="column_pipeline" name="pipeline" type="text"
      ucd="meta.id;meta.software" tablehead="Pipeline" verbLevel="1"
      description="The name of the pipeline used in the analysis, in upper case (ex: GSTLAL, PYCBC)."
    />
    <column id="column_network" name="network" type="text"
      ucd="meta.id;instr" tablehead="Network" verbLevel="1"
      description="The interferometers, whose data were used to produce the trigger, in alphabetic order (ex: H1,L1,V1)."
    />
    <column id="column_network_snr" name="network_snr" type="real"
      ucd="stat.snr" tablehead="Network SNR" verbLevel="1"
      description="The combined Signal-to-Noise ratio for the interferometers in the network."
    />
    <column id="column_H1_snr" name="H1_snr" type="real"
      ucd="stat.snr" tablehead="H1 SNR" verbLevel="1"
      description="The Signal-to-Noise ratio for the Hanford 1 interferometer."
    />
    <column id="column_L1_snr" name="L1_snr" type="real"
      ucd="stat.snr" tablehead="L1 SNR" verbLevel="1"
      description="The Signal-to-Noise ratio for the Livingston 1 interferometer."
    />
    <column id="column_V1_snr" name="V1_snr" type="real"
      ucd="stat.snr" tablehead="V1 SNR" verbLevel="1"
      description="The Signal-to-Noise ratio for the Virgo 1 interferometer."
    />
    <column id="column_ifar" name="ifar" type="real"
      ucd="time.interval;stat.falsePositive" unit="yr" tablehead="Inverse FAR" verbLevel="1"
      description="The inverse false alarm rate."
    />
    <column id="column_end_time_gps" name="end_time_gps" type="double precision"
      ucd="time.epoch" unit="s" tablehead="End Time GPS" verbLevel="1"
      description="The GPS time of the merger. Precise definition may differ, depending on the catalog."
    />
    <column id="column_p_astro" name="p_astro" type="real"
      ucd="stat.probability;src.class" tablehead="P-astro" verbLevel="1"
      description="The bayesian probability of being astrophysical (or not being terrestrial)."
    />
    <column id="column_mass_1" name="mass_1" type="real"
      ucd="phys.mass" unit="solMass" tablehead="Mass 1" verbLevel="1"
      description="The detector-frame mass of the heavier object in the binary."
    />
    <column id="column_mass_2" name="mass_2" type="real"
      ucd="phys.mass" unit="solMass" tablehead="Mass 2" verbLevel="1"
      description="The detector-frame mass of the lighter object in the binary."
    />
    <column id="column_spin_1z" name="spin_1z" type="real"
      ucd="phys.spinParameter" tablehead="Spin 1 (z)" verbLevel="1"
      description="The z-component of the heavier object's spin parameter in Euclidean coordinates."
    />
    <column id="column_spin_2z" name="spin_2z" type="real"
      ucd="phys.spinParameter" tablehead="Spin 2 (z)" verbLevel="1"
      description="The z-component of the lighter object's spin parameter in Euclidean coordinates."
    />
   </table>

  <table id="all_triggers" adql="True" onDisk="True">
    <meta name="title">Pipeline triggers from the GWTC-2.1, GWTC-3, IAS-O3a and 4-OGC data releases</meta>
    <meta name="description">
      The Pipeline Trigger Database contains triggers from 4 releases.
      Two come from the LVK collaboration: GWTC-2.1 and GWTC-3 (Candidate Data Release)
      and the other two releases are external to it: 4-OGC (Nitz, Alexander et al. 2021)
      and IAS-O3a (Olsen, Seth et al. 2022).
    </meta>
		<index columns="catalog"/>
		<index columns="end_time_gps"/>
    <column original="column_name"/>
    <column original="column_original_name"/>
    <column original="column_release"/>
    <column original="column_catalog"/>
    <column original="column_pipeline"/>
    <column original="column_network"/>
    <column original="column_network_snr"/>
    <column original="column_H1_snr"/>
    <column original="column_L1_snr"/>
    <column original="column_V1_snr"/>
    <column original="column_ifar"/>
    <column original="column_end_time_gps"/>
    <column original="column_p_astro"/>
    <column original="column_mass_1"/>
    <column original="column_mass_2"/>
    <column original="column_spin_1z"/>
    <column original="column_spin_2z"/>
  </table>

  <data id="import_all_triggers">
    <sources>data/all_triggers.csv.gz</sources>
    <csvGrammar preFilter="zcat">
      <rowfilter procDef="//products#define">
        <bind name="table">"schema.all_triggers"</bind>
      </rowfilter>
    </csvGrammar>
    <make table="all_triggers">
      <rowmaker idmaps="name,original_name,release,catalog,pipeline,network,network_snr,H1_snr,L1_snr,V1_snr,ifar,end_time_gps,p_astro,mass_1,mass_2,spin_1z,spin_2z"/>
    </make>
  </data>

</resource>

