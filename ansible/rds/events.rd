<resource schema="events">
  <meta name="title">Gravitational Wave Event Database</meta>
  <meta name="description">
  This resource includes the confident and marginal detections for the releases:
  GWTC-1, GWTC-2.1, GWTC-3 and O3 IMBH Marginal.
  </meta>
  <meta name="creationDate">2022-09-08T12:55:00</meta>
  <meta name="subject">gravitational waves</meta>
  <meta name="creator">Gravitation Wave Open Science Center</meta>
  <meta name="instrument">L1, H1, V1</meta>
  <meta name="facility">LIGO/VIRGO</meta>
  <meta name="source">https://gwosc.docs.ligo.org/igwn-tap</meta>
  <meta name="contentLevel">Research</meta>
  <meta name="type">Catalog</meta>

  <table>
    <column id="column_name" name="name" type="text"
      ucd="meta.id;meta.main" tablehead="Name" verbLevel="1"
      description="The unique identifier of the event."
    />
    <column id="column_release" name="release" type="text"
      ucd="meta.id" tablehead="Release" verbLevel="1"
      description="The data release in which the event has been published (ex: GWTC-2.1)."
    />
    <column id="column_catalog" name="quoted/catalog" type="text"
      ucd="meta.id" tablehead="Catalog" verbLevel="1"
      description="The data release catalog that contains the event (ex: GWTC-2.1-confident)."
    />
    <column id="column_network_matched_filter_snr" name="network_matched_filter_snr" type="real"
      ucd="stat.snr" tablehead="Network Matched Filter SNR" verbLevel="1"
      description="The matched filter signal to noise ratio in the gravitational wave detector network."
    />
    <column id="column_far" name="far" type="real"
      ucd="arith.rate;stat.falsePositive" unit="yr**-1" tablehead="FAR" verbLevel="1"
      description="The false alarm rate."
    />
    <column id="column_ifar" name="ifar" type="real"
      ucd="arith.interval;stat.falsePositive" unit="yr" tablehead="FAR⁻¹" verbLevel="1"
      description="The inverse false alarm rate."
    />
    <column id="column_geocent_time_gps" name="geocent_time_gps" type="double precision"
      ucd="time.epoch" unit="s" tablehead="Merger Time GPS" verbLevel="1"
      description="The GPS merger time at the geocenter."
    />
    <column id="column_p_astro" name="p_astro" type="real"
      ucd="stat.probability;src.class" tablehead="P-astro" verbLevel="1"
      description="The bayesian probability of being astrophysical (or not being terrestrial)."
    />
    <column id="column_luminosity_distance" name="luminosity_distance" type="real"
      ucd="pos.distance" unit="Mpc" tablehead="Lum. Distance" verbLevel="1"
      description="The luminosity distance of the source."
    />
    <column id="column_redshift" name="redshift" type="real"
      ucd="src.redshift" tablehead="Redshift" verbLevel="1"
      description="The redshift depending on specified cosmology."
    />
    <column id="column_mass_1_source" name="mass_1_source" type="real"
      ucd="phys.mass" unit="solMass" tablehead="Mass 1" verbLevel="1"
      description="The source-frame mass of the heavier object in the binary."
    />
    <column id="column_mass_2_source" name="mass_2_source" type="real"
      ucd="phys.mass" unit="solMass" tablehead="Mass 2" verbLevel="1"
      description="The source-frame mass of the lighter object in the binary."
    />
    <column id="column_total_mass_source" name="total_mass_source" type="real"
      ucd="phys.mass" unit="solMass" tablehead="Total Mass" verbLevel="1"
      description="The source-frame combined mass of the primary and secondary masses."
    />
    <column id="column_final_mass_source" name="final_mass_source" type="real"
      ucd="phys.mass" unit="solMass" tablehead="Final Mass" verbLevel="1"
      description="The source-frame remnant mass estimated using the spins evolved to the ISCO frequency."
    />
    <column id="column_chirp_mass" name="chirp_mass" type="real"
      ucd="phys.mass" unit="solMass" tablehead="Detector Chirp Mass" verbLevel="1"
      description="The detector-frame chirp mass."
    />
    <column id="column_chirp_mass_source" name="chirp_mass_source" type="real"
      ucd="phys.mass" unit="solMass" tablehead="Chirp Mass" verbLevel="1"
      description="The source-frame chirp mass."
    />
    <column id="column_chi_eff" name="chi_eff" type="real"
      ucd="phys.inspiralSpinParameter" tablehead="χeff" verbLevel="1"
      description="The effective inspiral spin parameter."
    />
    <column id="column_jsonurl" name="jsonurl" type="text"
      ucd="meta.ref.url" tablehead="JSON URL" verbLevel="1"
      description="The URL of the JSON data."
    />
    <column id="column_reference" name="reference" type="text"
      ucd="meta.ref" tablehead="Reference" verbLevel="1"
      description="The reference identifying the data release."
    />
    <column id="column_version" name="version" type="integer" required="true"
      ucd="meta.version" tablehead="Version" verbLevel="1"
      description="The version of the event entry."
    />
  </table>

  <table id="all_events" adql="True" onDisk="True">
    <meta name="title">All Gravitational Wave Events</meta>
    <meta name="description">
    The table all_events is the compilation of the confident and marginal detections for the following releases: GWTC-1, GWTC-2.1, GWTC-3 and O3 IMBH Marginal.
    </meta>
		<index columns="name"/>
		<index columns="geocent_time_gps"/>
    <column original="column_name"/>
    <column original="column_release"/>
    <column original="column_catalog"/>
    <column original="column_network_matched_filter_snr"/>
    <column original="column_far"/>
    <column original="column_ifar"/>
    <column original="column_geocent_time_gps"/>
    <column original="column_luminosity_distance"/>
    <column original="column_redshift"/>
    <column original="column_mass_1_source"/>
    <column original="column_mass_2_source"/>
    <column original="column_total_mass_source"/>
    <column original="column_final_mass_source"/>
    <column original="column_chirp_mass"/>
    <column original="column_chirp_mass_source"/>
    <column original="column_chi_eff"/>
    <column original="column_jsonurl"/>
    <column original="column_reference"/>
    <column original="column_version"/>
  </table>

  <data id="import_all_events">
    <sources>data/all_events.csv</sources>
    <csvGrammar>
      <rowfilter procDef="//products#define">
        <bind name="table">"schema.all_events"</bind>
      </rowfilter>
    </csvGrammar>
    <make table="all_events">
      <rowmaker idmaps="name,release,catalog,network_matched_filter_snr,far,ifar,geocent_time_gps,luminosity_distance,redshift,mass_1_source,mass_2_source,total_mass_source,final_mass_source,chirp_mass,chirp_mass_source,chi_eff,jsonurl,reference,version"/>
    </make>
  </data>

</resource>

