<resource schema="dc" resdir=".">

  <meta name="_intro" format="rst"><![CDATA[
    From this form, you can run ADQL queries on the IGWN event and trigger databases,
    in case you don't have TOPCAT or some other TAP client at hand.

    The available tables are listed in this table:

    +------------+------------------+----------------------------------------------------------------+
    | Schema     | Table            | Description                                                    |
    +============+==================+=========================================+======================+
    | events     | all_events       | The gravitational wave event compilation table. It contains    |
    |            |                  | events from the *GWTC-1*, *GWTC-2.1*, *GWTC-3* and             |
    |            |                  | the *O3 Intermediate Black Hole Marginal* data releases.       |
    +------------+------------------+----------------------------------------------------------------+
    | triggers   | all_triggers     | The gravitational wave pipeline trigger compilation table.     |
    |            |                  | It contains the triggers published in the *GWTC-2.1*, *GWTC-3*,|
    |            |                  | *4-OGC* and *O3b-IAS* data releases.                           |
    +------------+------------------+----------------------------------------------------------------+

    For more information on the project and the databases, please consult the `documentation <https://gwosc.docs.ligo.org/igwn-tap/>`_.
]]>
  </meta>
  <meta name="_bottominfo" format="rst">
    There is a fixed limit to 100000 rows on this service.  If this
    bugs you, use \RSTservicelink{tap}{TAP} (you should anyway).

    Here's a few queries you could try:

    * Inspect the event table

      ::

        SELECT * FROM events.all_events

    * Inspect the trigger table

      ::

        SELECT TOP 100 FROM triggers.all_triggers

    * Select an event by name

      ::

        SELECT *
        FROM events.all_events
        WHERE name = 'GW170817'

    * Select an event by GPS time

      ::

        SELECT *
        FROM events.all_events
        WHERE ABS(geocent_time_gps - 1187008882) &lt; 1

    * To match the pipeline triggers related to an event

      ::

        SELECT
            trig.*
        FROM
            triggers.all_triggers AS trig
            INNER JOIN events.all_events AS ev
            ON ABS(trig.end_time_gps - ev.geocent_time_gps) &lt; 1
        WHERE
            ev.name = 'GW170817'

    * To cross-match the triggers from all catalogs with an inverse FAR greater than 2 hours, within a time window of 1 second

      ::

        SELECT
            t1.name,
            t2.name,
            ABS(t1.end_time_gps - t2.end_time_gps) AS "Distance"
        FROM
            triggers.all_triggers AS t1
            INNER JOIN triggers.all_triggers AS t2
            ON ABS(t1.end_time_gps - t2.end_time_gps) &lt; 1
        WHERE
                t1.ifar &gt; 1. / 365 / 12
            AND t2.ifar &gt; 1. / 365 / 12
            AND t1.end_time_gps &lt; t2.end_time_gps

  </meta>

  <service id="q">
    <adqlCore>
      <inputTable id="adqlInput">
        <inputKey name="query" tablehead="ADQL query" type="text"
          description="A query in the Astronomical Data Query Language"
          widgetFactory="widgetFactory(ScalingTextArea, rows=6)"
          required="True"/>
        <inputKey name="_TIMEOUT" type="integer" unit="s"
          tablehead="Timeout after"
          description="Seconds until the query is aborted.  If you find
            yourself having to raise this beyond 200 or so, please contact
            the site operators for hints on how to optimize your query">
          <values default="5"/>
        </inputKey>
      </inputTable>
    </adqlCore>

    <meta name="shortName">adql</meta>
    <meta name="title">ADQL Query Form</meta>
    <meta name="subject">gravitational waves</meta>
    <meta name="description">ADQL query form for the event and trigger IGWN databases.</meta>
    <meta name="creationDate">2022-12-07T18:01:00</meta>
    <publish render="form" sets="local"/>
  </service>

</resource>

